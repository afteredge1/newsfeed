<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\FiltersController;
use App\Http\Controllers\Api\V1\PreferenceController;
use App\Http\Controllers\Api\V1\SearchController;
use App\Http\Controllers\API\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {
    
    // Public routes
    Route::post('/register', [UserController::class, 'store']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/filters', [FiltersController::class, 'getFilters']);
    Route::post('/search', [SearchController::class, 'index']);
    

    // Routes that require authentication
    Route::middleware('auth:sanctum')->group(function () {
        
        // User-related routes
        Route::get('/user', [AuthController::class, 'show']);
        Route::post('/logout', [AuthController::class, 'logout']);

        // Get all user subscribed news channels
        Route::get('/user/feed', [PreferenceController::class, 'userFeed']);
    });
});
