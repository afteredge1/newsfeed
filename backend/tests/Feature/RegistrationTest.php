<?php

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanRegister()
    {
        $userData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];

        $response = $this->post('/register', $userData);

        $response->assertStatus(302); // or any expected status code
        $this->assertDatabaseHas('users', ['email' => 'johndoe@example.com']);
    }
}
