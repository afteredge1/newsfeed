<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('news_channel_id');
            $table->string('title');
            $table->string('abstract');
            $table->longText('details');
            $table->string('type');
            $table->string('web_url');
            $table->string('published_at')->nullable();
            $table->string('img');
            $table->string('api_source');
            $table->json('authors')->nullable();
            $table->timestamps();

            // Foreign key constraint
            $table->foreign('news_channel_id')->references('id')->on('news_channels');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('articles');
    }
};
