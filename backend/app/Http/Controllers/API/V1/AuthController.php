<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Services\AuthService;
use Exception;

class AuthController extends Controller 
{
    public function __construct(private AuthService $authService) {}

    public function login(LoginRequest $request) 
    {
        try {
            $credentials = $request->only('email', 'password');
            $token = $this->authService->login($credentials);
            return response()->json(['token' => $token], 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'Failed to login', 'message' => $e->getMessage()], 500);
        }
    }

    public function logout() 
    {
        try {
            $this->authService->logout();
            return response()->json(['message' => 'Logged out successfully'], 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'Failed to logout', 'message' => $e->getMessage()], 500);
        }
    }
}
