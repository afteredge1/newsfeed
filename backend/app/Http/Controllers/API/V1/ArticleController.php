<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct(private ArticleService $articleService) {}

    public function search(Request $request) 
    {
        $articles = $this->articleService->searchAndFilter($request->all());
        return response()->json($articles);
    }
}
