<?php
namespace App\Http\Controllers;

use App\Services\FilterService;

class FiltersController extends Controller 
{
    protected $filterService;

    public function __construct(FilterService $filterService) {
        $this->filterService = $filterService;
    }

    public function getFilters() {
        $filters = $this->filterService->getAllFilters();
        return response()->json($filters);
    }
}
