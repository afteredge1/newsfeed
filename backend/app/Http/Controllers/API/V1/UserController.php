<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller 
{
    public function __construct(private UserService $userService) {}

    public function store(UserRequest $request): JsonResponse
    {
        try {
            $data = $request->validated();
            $user = $this->userService->create($data);
            return response()->json($user, 201);
        } catch (Exception $e) {
            return response()->json(['error' => 'Failed to create the user', 'message' => $e->getMessage()], 500);
        }
    }

    public function show(): JsonResponse 
    {
        try {
            $user = Auth::user();
            
            if (!$user) {
                return response()->json(['error' => 'No authenticated user found'], 401);
            }

            return response()->json($user, 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'Failed to get the user', 'message' => $e->getMessage()], 500);
        }
    }
}
