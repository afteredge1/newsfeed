<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\PreferenceService;
use Illuminate\Http\Request;

class PreferenceController extends Controller 
{
    public function __construct(private PreferenceService $preferenceService) {}

    public function userFeed(Request $request) 
    {
        $articles = $this->preferenceService->getUserPreferredArticles($request->user()->id);
        return response()->json($articles);
    }
}
