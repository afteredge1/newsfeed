<?php

namespace App\Repositories;

use App\Models\Preferences;

class PreferenceRepository 
{
    public function getUserPreferences($userId)
    {
        return Preferences::where('user_id', $userId)->pluck('article_id');
    }
}
