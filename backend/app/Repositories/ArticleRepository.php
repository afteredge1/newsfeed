<?php

namespace App\Repositories;

use App\Models\Articles;

class ArticleRepository 
{
    public function searchAndFilter($params)
    {
        $query = Articles::query();

        if (isset($params['keyword'])) {
            $query->where('title', 'LIKE', "%{$params['keyword']}%");
        }

        if (isset($params['date'])) {
            $query->whereDate('published_at', $params['date']);
        }

        if (isset($params['source'])) {
            $query->whereHas('newsChannel', function ($query) use ($params) {
                $query->where('news_channel_name', $params['source']);
            });
        }

        return $query->get();
    }
}
