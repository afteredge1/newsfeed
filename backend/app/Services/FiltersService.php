<?php
namespace App\Services;

use App\Models\Articles;

class FiltersService
{
    function getSources(): array
    {
        return Articles::select('api_source')->distinct()->get()->toArray();
    }

    function getTypes(): array
    {
        return Articles::select('type')->distinct()->get()->toArray();
    }
}
