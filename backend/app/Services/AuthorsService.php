<?php

namespace App\Services;

use App\Models\Articles;
use Illuminate\Support\Facades\DB;

class AuthorsService
{
    public function getAuthors(): array
    {
        return Articles::select('id', DB::raw('authors->"$.name" as name'))->get()->toArray();

    }
}
