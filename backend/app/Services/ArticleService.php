<?php

namespace App\Services;

use App\Repositories\ArticleRepository;

class ArticleService 
{
    public function __construct(private ArticleRepository $articleRepository) {}

    public function searchAndFilter($params)
    {
        return $this->articleRepository->searchAndFilter($params);
    }
}
