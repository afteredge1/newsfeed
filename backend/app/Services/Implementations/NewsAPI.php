<?php

namespace App\Services\Implementations;

use App\DTOs\NewsAiDTO;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class NewsAPI
{
    protected $apiData;
    protected $config;
    public function __construct(array $config) {
        $this->config = $config;
        $this->apiData = collect();
    }

    public function fetchArticles(): array
    {
        $this->getArticlesFromApi();
        return $this->normalizeData();
    }

    protected function getArticlesFromApi()
    {
        $url = $this->config['base_url'];
        $apiKey = $this->config['api_key'];

        $params = [
            'api-key' => $apiKey,
            'begin_date' => Carbon::yesterday()->toDateString(),
            'end_date' => Carbon::today()->toDateString(),
            'page' => 1,
            'page-size' => 100
        ];

        $headers = [
            'Content-Type' => 'application/json',
        ];

        do {
            $queryString = http_build_query($params);
            $response = Http::withHeaders($headers)->get($url . '?' . $queryString);

            if ($response->successful()) {
                $responseData = $response->json();
                $docs = data_get($responseData, 'response.docs', []);
                $this->apiData = $this->apiData->concat($docs);
                
                $totalHits = data_get($responseData, 'response.meta.hits', 0);
                $totalPages = ceil($totalHits / $params['page-size']);

                $params['page']++;
            } else {
                Log::error("API request failed with status code: " . $response->status());
                break;
            }
        } while ($params['page'] <= $totalPages);
    }

    protected function normalizeData(): array
    {
        return $this->apiData->map(fn($data) => new NewsAiDTO($data))->toArray();
    }
}
