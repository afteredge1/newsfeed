<?php

namespace App\Services;

use App\Models\Articles;
use App\Repositories\PreferenceRepository;

class PreferenceService 
{
    public function __construct(private PreferenceRepository $preferenceRepository) {}

    public function getUserPreferredArticles($userId)
    {
        $articleIds = $this->preferenceRepository->getUserPreferences($userId);
        return Articles::whereIn('id', $articleIds)->get();
    }
}
