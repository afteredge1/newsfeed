<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthService 
{
    public function __construct(private UserRepository $userRepository) {}

    public function login(array $credentials): string
    {
        $user = $this->userRepository->getByEmail($credentials['email']);

        if (!$user || !Hash::check($credentials['password'], $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return $this->userRepository->createToken($user, 'api-token');
    }

    public function logout(): void
    {
        $user = Auth::user();
        $this->userRepository->deleteCurrentToken($user);
    }
}
