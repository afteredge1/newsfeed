<?php

namespace App\Services;

use App\Models\Articles;
use App\Services\Implementations\NewsAPI;
use Exception;

class NewsService
{
    protected $service;

    public function __construct(string $type)
    {
        switch ($type) {
            case 'newsapi':
                $config = config('newsapis.newsapi');
                $this->service = new NewsAPI($config);
                break;
            case 'nytimes':
                $config = config('newsapis.nytimes');
                $this->service = new NewsAPI($config);
                break;
            // Handle other types...
            default:
                throw new \Exception("Invalid service type provided");
        }
    }

    public function fetchArticles(): array
    {
        $articles = $this->service->fetchArticles();
        
        // store in database
        try {
            Articles::insert($articles);
        } catch(\Illuminate\Database\QueryException $e) { 
            return $e->getMessage();
        }
        
        return $articles;
    }
}
