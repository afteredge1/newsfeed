
# Newsfeed App

## Overview

The Newsfeed App serves as a RESTful API that empowers users to curate and administer news feeds. It's architected using Laravel in tandem with MySQL. For a seamless user experience, the frontend leverages the capabilities of REACT. To simplify deployment and set-up, the application has been dockerized. 

This documentation provides a holistic view of the application blueprint and how the backend and frontend codes intertwine. Please note that while this may not be a fully operational application, its primary intent is to showcase its structural design.

To populate the database with articles from various news sources, use the command:
```bash
php artisan app:fetch-articles
```
Dedicated jobs will be established for each news channel type. Their respective configurations can be found in `config/newsapi.php`.

Basic feature tests have been incorporated primarily for illustrative purposes. Due to time constraints, they offer a snapshot rather than an exhaustive exploration.

### Design Patterns:

1. **Repository Pattern**: 
    - **Purpose**: To abstract the data layer, making our application more flexible to maintain. It acts as a middle-man between the data source (like a database) and the business logic layer of the application.
    - **Benefits**:
        - Centralized data access logic, making code easier to maintain.
        - Decouples the application from persistence frameworks or storage mechanisms.
        - Makes the application more testable since the domain logic can be tested separately from database access code.

2. **Service Pattern**: 
    - **Purpose**: To isolate application logic from the user interface. Acting as an intermediary, it also allows shared logic to be reused.
    - **Benefits**:
        - Provides a set of reusable functions across different parts of the application.
        - Isolates domain logic, making it easier to update or refactor.
        - Offers a centralized point for external integrations.

These design patterns are implemented according to **SOLID** principles.

## Prerequisites

- Docker & Docker Compose installed
- Postman or another API client (for testing endpoints)

## Setup

### 1. Build and Spin Up Docker Containers

Execute the following commands from the root directory of the project:

```bash
docker-compose build
docker-compose up -d
```


### 2. Database Setup

Create the necessary databases:

- `notebook`: Main database for application data.
- `notebook_test`: Database for testing purposes.

**Note**: Depending on the database container configuration, you might have to log in and create these databases manually.

### 3. Migrate Database

To set up the necessary database tables, execute:

```bash
docker-compose exec web php artisan migrate
```


## Usage

## Base URL

All endpoints are prefixed with:
```
http://localhost:8080/api/v1
```

## Public Endpoints

### Login

**Endpoint**: `/login`

**Method**: `POST`

**Description**: Authenticate the user and return an access token.

**Payload**:
```json
{
    "email": "user@example.com",
    "password": "userpassword"
}
```

**Response**:
- **Success**: Returns an access token.
- **Error**: Descriptive error message.

---

## Authenticated Endpoints

These endpoints require an authentication token to be provided in the request header.

### Fetch Authenticated User

**Endpoint**: `/user`

**Method**: `GET`

**Description**: Retrieve the details of the authenticated user.

**Response**:
- **Success**: Returns the details of the authenticated user.
- **Error**: Descriptive error message.

### Logout

**Endpoint**: `/logout`

**Method**: `POST`

**Description**: Log out the authenticated user, invalidating their token.

**Response**:
- **Success**: A success message confirming the user has been logged out.
- **Error**: Descriptive error message.


### Tests

Tests are incorporated a suite of tests to ensure the robustness and reliability of our application, covering critical areas like User management, Notebook functionality, and Note operations.

```bash
docker-compose exec web php artisan test
```

### Frontend

### Article Search

#### Access
Visit `http://localhost:3000/` and navigate to the search panel.

#### Features:

- **Dynamic Search Bar**: Begin typing to see real-time article matches.

- **Filtering Options**: Streamline your search results:
  - `Categories`: Pick one or multiple categories to tailor results.
  - `Sources`: Select from our list of sources to get articles specific to them.

- **Search Results**: Articles are displayed in an organized card layout with relevant information like title, brief description, source, and more.

### User Dashboard

#### Access
After logging in, you'll be redirected to your personal dashboard.

#### Features:

- **Profile Overview**: View your basic profile information at a glance.

- **Preferences Panel**: Customize your news feed experience:
  - `News Sources`: Select your preferred news outlets.

- **Feedback & Settings**: A one-stop for tweaking your account settings or sending us feedback about your experience.


