import axios from "axios";
import { LOGIN_ENDPOINT, SIGNUP_ENDPOINT, LOGOUT_ENDPOINT } from '../config/constants';

export const loginUser = (email, password) => 
    axios.post(LOGIN_ENDPOINT, { email, password });

export const signupUser = (name, email, password) => 
    axios.post(SIGNUP_ENDPOINT, { name, email, password });

export const logoutUser = (token) => 
    axios.post(LOGOUT_ENDPOINT, {}, {
        headers: { Authorization: `Bearer ${token}` }
    });
