import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

export const createPersistedReducer = (key, reducer) => {
  const persistConfig = {
    key,
    storage,
  };

  return persistReducer(persistConfig, reducer);
};
