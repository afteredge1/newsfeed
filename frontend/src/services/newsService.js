import axios from "axios";
import { API_URL } from '../config/constants';

export const searchNews = async ({ page, keywords, types, sources, authors }) => {
  const response = await axios.post(
    `${API_URL}/search?page=${page}`,
    {
      keywords,
      types,
      sources,
      authors,
    }
  );
  return response.data.data;
};

export const fetchFilters = async () => {
  const response = await axios.get(`${API_URL}/filters`);
  return response.data.data;
};
