
export const API_URL = process.env.REACT_APP_API_URL;

export const LOGIN_ENDPOINT = `${API_URL}/login`;
export const SIGNUP_ENDPOINT = `${API_URL}/register`;
export const LOGOUT_ENDPOINT = `${API_URL}/logout`;
