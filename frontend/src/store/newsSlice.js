import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import * as newsService from '../services/newsService';

export const fetchSearchResults = createAsyncThunk(
  "news/search",
  async (params, thunkAPI) => {
    try {
      return await newsService.searchNews(params);
    } catch (error) {
      throw new Error("Search failed.");
    }
  }
);

export const fetchFilterOptions = createAsyncThunk(
  "news/filters",
  async (_, thunkAPI) => {
    try {
      return await newsService.fetchFilters();
    } catch (error) {
      throw new Error("Fetching filter options failed.");
    }
  }
);


const initialState = {
    loading: false,
    error: null,
    data: [],
    options: {
        soruces: [],
        types: [],
        authors: [],
    },
};

const newsSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchSearchResults.pending, (state) => {
            state.loading = true;
            state.error = null;
        })
        .addCase(fetchSearchResults.fulfilled, (state, action) => {
            state.loading = false;
            state.data = action.payload;
        })
        .addCase(fetchSearchResults.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
        })
        .addCase(fetchFilterOptions.pending, (state) => {
            state.loading = true;
            state.error = null;
        })
        .addCase(fetchFilterOptions.fulfilled, (state, action) => {
            state.loading = false;
            state.options = action.payload;
        })
        .addCase(fetchFilterOptions.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
        })
      },
});

export const newsActions = newsSlice.actions;
export default newsSlice;
