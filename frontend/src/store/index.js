import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { persistStore } from "redux-persist";
import authSlice from "./authSlice";
import newsSlice from "./newsSlice";
import { createPersistedReducer } from '../services/persistService';

const rootReducer = combineReducers({
  auth: createPersistedReducer("auth", authSlice.reducer),
  news: newsSlice.reducer,
});

const store = configureStore({
  reducer: rootReducer,
});

const persistor = persistStore(store);

export { store, persistor };
