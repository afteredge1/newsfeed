import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { loginUser, signupUser, logoutUser } from '../services/apiService';

export const loginAsync = createAsyncThunk(
  "auth/login",
  async ({ email, password }) => {
    const response = await loginUser(email, password);
    return response.data;
  }
);

export const signupAsync = createAsyncThunk(
  "auth/signup",
  async ({ name, email, password }) => {
    const response = await signupUser(name, email, password);
    return response.data.user;
  }
);

export const logoutAsync = createAsyncThunk(
  "auth/logout",
  async (_, { getState }) => {
    const state = getState();
    const token = state.auth.user?.token;
    if (!token) {
      throw new Error("Bearer token not found.");
    }

    const response = await logoutUser(token);
    return response.data;
  }
);

const initialState = {
  loading: false,
  error: null,
  isAuthenticated: false,
  user: null,
  isCreated: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
    .addCase(loginAsync.pending, (state) => {
      state.loading = true;
      state.error = null;
    })
    .addCase(loginAsync.fulfilled, (state, action) => {
      state.loading = false;
      state.isAuthenticated = true;
      state.user = action.payload.user;
      alert("Login successful");
    })
    .addCase(loginAsync.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    })
    .addCase(signupAsync.pending, (state) => {
      state.loading = true;
      state.error = null;
    })
    .addCase(signupAsync.fulfilled, (state, action) => {
      state.loading = false;
      state.isCreated = true;
      alert("Signup successful");
    })
    .addCase(signupAsync.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    })
    .addCase(logoutAsync.pending, (state) => {
      state.loading = true;
      state.error = null;
    })
    .addCase(logoutAsync.fulfilled, (state, action) => {
      state.loading = false;
      state.isAuthenticated = false;
      state.user = null; 
      state.error = null; 
    })
    .addCase(logoutAsync.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

  },
});

export const authActions = authSlice.actions;
export default authSlice;
